// SPDX-License-Identifier: MIT

package storage

import (
	"context"
	"io"

	gcs "cloud.google.com/go/storage"
)

// Storage interface to deal with Google Cloud Storage. This interface is here to allow for testing
// to substitute something different.
type Storage interface {
	ReaderForObject(ctx context.Context, bucketID string, objectID string) (io.ReadCloser, error)
	WriterForObject(ctx context.Context, bucketID string, objectID string) (io.WriteCloser, error)
	DeleteObject(ctx context.Context, bucketID string, objectID string) error
}

// The one Storage object
var theStorage Storage = nil

// Set the storage object to use
func Set(storage Storage) {
	theStorage = storage
}

// Get the Storage object to use
func Get() Storage {
	return theStorage
}

const (
	// EventFinalize is the GCS notification event type for object finalization (creation or update)
	EventFinalize string = "OBJECT_FINALIZE"

	// EventDelete is the GCS notification event type for object deletion
	EventDelete string = "OBJECT_DELETE"
)

// The real implementation of the Storage interface that works with Google Cloud Storage.
type storageImpl struct {
	client *gcs.Client
}

// New creates a real implementation of the Storage interface
func New() (Storage, error) {
	var err error
	storage := &storageImpl{}

	ctx := context.Background()
	storage.client, err = gcs.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	return storage, nil
}

// ReaderForObject creates a new Reader for the given object in a bucket
func (storage storageImpl) ReaderForObject(ctx context.Context, bucketID string, objectID string) (io.ReadCloser, error) {
	return storage.client.Bucket(bucketID).Object(objectID).NewReader(ctx)
}

// WriterForObject creates a new Writer for the given object in a bucket
func (storage storageImpl) WriterForObject(ctx context.Context, bucketID string, objectID string) (io.WriteCloser, error) {
	return storage.client.Bucket(bucketID).Object(objectID).NewWriter(ctx), nil
}

// DeleteObject deletes the object at the given path in the bucket
func (storage storageImpl) DeleteObject(ctx context.Context, bucketID string, objectID string) error {
	return storage.client.Bucket(bucketID).Object(objectID).Delete(ctx)
}
