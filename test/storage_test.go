// SPDX-License-Identifier: MIT

package test_test

import (
	"context"
	"io/ioutil"

	gcs "cloud.google.com/go/storage"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/lookoutpoint/storage"
	. "gitlab.com/lookoutpoint/storage/test"
)

var _ = Describe("Storage", func() {
	ctx := context.Background()

	It("Store and read object", func() {
		s := New()

		// Write to an object path
		w, err := s.WriterForObject(ctx, "bucket", "path/to/file.txt")
		Expect(err).ToNot(HaveOccurred())

		payload := "test string"
		_, err = w.Write([]byte(payload))
		Expect(err).ToNot(HaveOccurred())

		w.Close()

		// Read from object path
		r, err := s.ReaderForObject(ctx, "bucket", "path/to/file.txt")
		Expect(err).ToNot(HaveOccurred())

		rbytes, err := ioutil.ReadAll(r)
		Expect(err).ToNot(HaveOccurred())
		r.Close()

		Expect(string(rbytes)).To(Equal(payload))
	})

	It("Invalid read object", func() {
		s := New()

		_, err := s.ReaderForObject(ctx, "bucket", "non-existent.jpg")
		Expect(err).To(HaveOccurred())

		// Same path as in the other test (but New() should reset)
		_, err = s.ReaderForObject(ctx, "bucket", "path/to/file.txt")
		Expect(err).To(HaveOccurred())
	})

	It("CopyLocalIntoStorage", func() {
		s := New()
		storage.Set(s)

		err := CopyLocalIntoStorage(ctx, "./fixtures/test.txt", "some-bucket", "path/to/test-file.txt")
		Expect(err).ToNot(HaveOccurred())

		// Read object
		r, err := s.ReaderForObject(ctx, "some-bucket", "path/to/test-file.txt")
		Expect(err).ToNot(HaveOccurred())

		rbytes, err := ioutil.ReadAll(r)
		Expect(err).ToNot(HaveOccurred())

		Expect(string(rbytes)).To(Equal("asdf blah"))
	})

	It("DeleteObject", func() {
		s := New()

		// Delete non-existent object
		err := s.DeleteObject(ctx, "bucket", "file.txt")
		Expect(err).To(Equal(gcs.ErrObjectNotExist))

		// Add object
		err = CopyLocalIntoStorage(ctx, "./fixtures/test.txt", "bucket", "file.txt")
		Expect(err).ToNot(HaveOccurred())

		// Get reader for object (should succeed)
		r, err := s.ReaderForObject(ctx, "bucket", "file.txt")
		Expect(err).ToNot(HaveOccurred())
		r.Close()

		// Delete object
		err = s.DeleteObject(ctx, "bucket", "file.txt")
		Expect(err).ToNot(HaveOccurred())

		// Try to get reader for object (should fail)
		_, err = s.ReaderForObject(ctx, "bucket", "file.txt")
		Expect(err).To(HaveOccurred())
	})
})
