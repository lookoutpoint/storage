// SPDX-License-Identifier: MIT

package test

import (
	"context"
	"io"
	"os"
	"path"

	gcs "cloud.google.com/go/storage"
	"gitlab.com/lookoutpoint/storage"
)

// Test implementation of Storage interface that reads from fixtures/storage
type testImpl struct {
	root string
}

// Test implementation of Reader interface that reads from fixtures/storage
type readerTestImpl struct {
	file *os.File
}

// Test implementation of Writer interface that writes to fixtures/storage
type writerTestImpl struct {
	file *os.File
}

// New creates a new test implementation of the Storage interface
func New() storage.Storage {
	s := &testImpl{root: "./.test/storage"}
	s.init()
	return s
}

func (storage testImpl) init() {
	// Clean up any existing content
	err := os.RemoveAll(storage.root)
	if err != nil {
		panic(err)
	}

	err = os.MkdirAll(storage.root, os.ModePerm)
	if err != nil {
		panic(err)
	}
}

func (storage testImpl) filePath(bucketID string, objectID string) string {
	return storage.root + "/" + bucketID + "/" + objectID
}

func (storage testImpl) ReaderForObject(ctx context.Context, bucketID string, objectID string) (io.ReadCloser, error) {
	var reader readerTestImpl
	var err error

	filePath := storage.filePath(bucketID, objectID)
	reader.file, err = os.Open(filePath)

	if err != nil {
		return nil, err
	}

	return reader, nil
}

func (storage testImpl) WriterForObject(ctx context.Context, bucketID string, objectID string) (io.WriteCloser, error) {
	var writer writerTestImpl
	var err error

	filePath := storage.filePath(bucketID, objectID)

	// Create directory path as needed
	dir := path.Dir(filePath)
	err = os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		return nil, err
	}

	writer.file, err = os.Create(filePath)

	if err != nil {
		return nil, err
	}

	return writer, nil
}

func (storage testImpl) DeleteObject(ctx context.Context, bucketID string, objectID string) error {
	filePath := storage.filePath(bucketID, objectID)
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return gcs.ErrObjectNotExist
	}

	return os.Remove(filePath)
}

func (reader readerTestImpl) Read(p []byte) (int, error) {
	return reader.file.Read(p)
}

func (reader readerTestImpl) Close() error {
	return reader.file.Close()
}

func (writer writerTestImpl) Write(p []byte) (int, error) {
	return writer.file.Write(p)
}

func (writer writerTestImpl) Close() error {
	return writer.file.Close()
}

// CopyLocalIntoStorage copies a local file into the destination storage path
func CopyLocalIntoStorage(ctx context.Context, localPath string, destBucket string, destPath string) error {
	localFile, err := os.Open(localPath)
	if err != nil {
		return err
	}
	defer localFile.Close()

	destWriter, err := storage.Get().WriterForObject(ctx, destBucket, destPath)
	if err != nil {
		return err
	}
	defer destWriter.Close()

	_, err = io.Copy(destWriter, localFile)
	return err
}
