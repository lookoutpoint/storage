# storage

Simple wrapper around Google Cloud Storage APIs to provide an interface for testing.

## License

MIT License. [Full license](LICENSE)
